package db

import (
	"database/sql"

	"bitbucket.org/michal_hartwich/simpleblog/models"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

type Database struct {
	*sql.DB
}

type Repository interface {
	GetPosts() ([]*models.Post, error)
	GetPost(id int) (*models.Post, error)
	CreatePost(post *models.Post) (*models.Post, error)
	UpdatePost(post *models.Post) (*models.Post, error)
	DeletePost(id int) error

	CreateUser(user *models.User) (*models.User, error)
	GetUserByEmail(email string) (*models.User, error)
	GetUserByToken(token string) (*models.User, error)
	SetUserToken(id int, token string) error

	Close() error
}

func New(config *Config) (*Database, error) {
	db, err := sql.Open("postgres", config.ConnectionString)
	if err != nil {
		return nil, errors.Wrap(err, "unable to connect to database")
	}
	return &Database{db}, nil
}

func (db *Database) Close() error {
	return db.Close()
}
