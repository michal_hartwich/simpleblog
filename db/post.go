package db

import (
	"database/sql"

	"bitbucket.org/michal_hartwich/simpleblog/models"
	sq "github.com/Masterminds/squirrel"
)

func (db *Database) GetPosts() ([]*models.Post, error) {
	query := sq.Select("*").
		From("posts").
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	payload := make([]*models.Post, 0)

	rows, err := query.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		data := new(models.Post)

		err := rows.Scan(
			&data.ID,
			&data.Title,
			&data.Content,
			&data.UserID,
		)
		if err != nil {
			return nil, err
		}

		payload = append(payload, data)
	}

	if len(payload) == 0 {
		return nil, sql.ErrNoRows
	}

	return payload, nil
}

func (db *Database) GetPost(id int) (*models.Post, error) {
	query := sq.Select("*").
		From("posts").
		Where(sq.Eq{"id": id}).
		RunWith(db).
		PlaceholderFormat(sq.Dollar)
	post := &models.Post{}
	err := query.QueryRow().Scan(
		&post.ID,
		&post.Title,
		&post.Content,
		&post.UserID,
	)
	if err != nil {
		return nil, err
	}
	return post, nil
}

func (db *Database) CreatePost(post *models.Post) (*models.Post, error) {
	query := sq.Insert("posts").
		Columns("title", "content", "user_id").
		Values(post.Title, post.Content, post.UserID).
		Suffix("RETURNING \"id\"").
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	err := query.QueryRow().Scan(&post.ID)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (db *Database) UpdatePost(post *models.Post) (*models.Post, error) {
	query := sq.Update("posts").
		Set("title", post.Title).
		Set("content", post.Content).
		Set("user_id", post.UserID).
		Where(sq.Eq{"id": post.ID}).
		//Where("id", post.ID).
		Suffix("RETURNING \"id\"").
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	err := query.QueryRow().Scan(&post.ID)
	if err != nil {
		return nil, err
	}
	return post, nil
}

func (db *Database) DeletePost(id int) error {
	query := sq.Delete("posts").
		Where(sq.Eq{"id": id}).
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	res, err := query.Exec()
	if err != nil {
		return err
	}
	if ra, _ := res.RowsAffected(); ra == 0 {
		return sql.ErrNoRows
	}
	return nil
}
