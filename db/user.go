package db

import (
	"bitbucket.org/michal_hartwich/simpleblog/models"
	sq "github.com/Masterminds/squirrel"
)

func (db *Database) CreateUser(user *models.User) (*models.User, error) {
	query := sq.Insert("users").
		Columns("email", "password").
		Values(user.Email, user.Password).
		Suffix("RETURNING \"id\"").
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	err := query.QueryRow().Scan(&user.ID)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *Database) GetUserByEmail(email string) (*models.User, error) {
	query := sq.Select("id, email, password, COALESCE(token, '')").
		From("users").
		Where(sq.Eq{"email": email}).
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	user := &models.User{}

	err := query.QueryRow().Scan(
		&user.ID,
		&user.Email,
		&user.Password,
		&user.Token,
	)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *Database) GetUserByToken(token string) (*models.User, error) {
	query := sq.Select("id, email, token").
		From("users").
		Where(sq.Eq{"token": token}).
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	user := &models.User{}

	err := query.QueryRow().Scan(
		&user.ID,
		&user.Email,
		&user.Token,
	)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *Database) SetUserToken(id int, token string) error {
	query := sq.Update("users").
		Set("token", token).
		Where(sq.Eq{"id": id}).
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	_, err := query.Query()
	if err != nil {
		return err
	}
	return nil
}
