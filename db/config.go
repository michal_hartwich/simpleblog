package db

import (
	"fmt"
	"os"
)

type Config struct {
	ConnectionString string
}

func InitConfig() (*Config, error) {
	config := &Config{
		ConnectionString: os.Getenv("DB_CONN_STR"),
	}
	if config.ConnectionString == "" {
		return nil, fmt.Errorf("Database ConnectionString must be set")
	}
	return config, nil
}
