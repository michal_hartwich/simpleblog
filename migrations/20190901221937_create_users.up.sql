CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY NOT NULL,
    email VARCHAR(300),
    password VARCHAR(300),
    token VARCHAR(300)
);

CREATE INDEX idx_users_email ON users(email);
CREATE INDEX idx_users_token ON users(token);