module bitbucket.org/michal_hartwich/simpleblog

go 1.12

require (
	github.com/Masterminds/squirrel v1.1.0
	github.com/NYTimes/gziphandler v1.1.1
	github.com/alexflint/go-filemutex v0.0.0-20171028004239-d358565f3c3f // indirect
	github.com/containerd/containerd v1.2.7 // indirect
	github.com/golang-migrate/migrate/v4 v4.6.0
	github.com/gorilla/mux v1.7.3
	github.com/joho/godotenv v1.3.0
	github.com/khaiql/dbcleaner v2.3.0+incompatible
	github.com/lib/pq v1.2.0
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/morikuni/aec v0.0.0-20170113033406-39771216ff4c // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.4.0
	golang.org/x/crypto v0.0.0-20190426145343-a29dc8fdc734
	golang.org/x/sys v0.0.0-20190801041406-cbf593c0f2f3 // indirect
	gopkg.in/khaiql/dbcleaner.v2 v2.3.0
	gopkg.in/testfixtures.v2 v2.5.3
)
