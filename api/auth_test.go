package api_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"regexp"
	"testing"
)

func TestAPI_CreateUser(t *testing.T) {
	loadEnv()
	prepareDb()
	cleanTable("users")

	t.Run("with valid params", func(t *testing.T) {
		payload := map[string]string{
			"email":                 "user@example.com",
			"password":              "userpass",
			"password_confirmation": "userpass",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/auth/register", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		expected := `{"id":\d+,"email":"user@example.com"}`
		r, _ := regexp.Compile(expected)

		if !r.MatchString(rr.Body.String()) {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("with invalid params", func(t *testing.T) {
		payload := map[string]string{
			"email":                 "",
			"password":              "aaa",
			"password_confirmation": "bbb",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/auth/register", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusBadRequest)
		}

		expected := `{"message":"Bad request","errors":{"email":["Email param cannot be empty"],"password":["Password and password confirmation don't match"]}}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when user exists", func(t *testing.T) {
		loadFixtures()
		payload := map[string]string{
			"email":                 "user@example.com",
			"password":              "aaa",
			"password_confirmation": "aaa",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/auth/register", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusUnprocessableEntity {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusUnprocessableEntity)
		}

		expected := `{"message":"User with given email already exists"}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})
}

func TestAPI_LoginUser(t *testing.T) {
	loadEnv()
	prepareDb()
	cleanTable("users")
	loadFixtures()

	t.Run("when user exists", func(t *testing.T) {
		payload := map[string]string{
			"email":    "user@example.com",
			"password": "userpass",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		expected := `{"token":"\w+"}`
		r, _ := regexp.Compile(expected)

		if !r.MatchString(rr.Body.String()) {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when password invalid", func(t *testing.T) {
		payload := map[string]string{
			"email":    "user@example.com",
			"password": "userpass_wrong",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusUnauthorized {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusUnauthorized)
		}

		expected := `{"message":"Invalid email or password"}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when invalid request", func(t *testing.T) {
		payload := map[string]string{
			"email":    "",
			"password": "",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusBadRequest)
		}

		expected := `{"message":"Bad request","errors":{"email":["Email param cannot be empty"],"password":["Password param cannot be empty"]}}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})
}
