package api_test

import (
	"database/sql"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"

	"bitbucket.org/michal_hartwich/simpleblog/api"
	"bitbucket.org/michal_hartwich/simpleblog/app"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/khaiql/dbcleaner"
	"github.com/khaiql/dbcleaner/engine"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gopkg.in/testfixtures.v2"
)

var cleaner dbcleaner.DbCleaner

func loadEnv() {
	err := godotenv.Load("../.env.test")
	if err != nil {
		logrus.Fatal("Error loading .env file")
	}
}

func loadFixtures(files ...string) {
	db, err := sql.Open("postgres", os.Getenv("DB_CONN_STR"))
	if err != nil {
		_ = errors.Wrap(err, "unable to connect to database")
		return
	}

	var fixtures *testfixtures.Context
	if len(files) > 0 {
		fls := make([]string, 0)
		for _, val := range files {
			fls = append(fls, fmt.Sprintf("fixtures/%s.yml", val))
		}
		fixtures, err = testfixtures.NewFiles(db, &testfixtures.PostgreSQL{}, fls...)
	} else {
		fixtures, err = testfixtures.NewFolder(db, &testfixtures.PostgreSQL{}, "fixtures")
	}

	err = fixtures.Load()
	if err != nil {
		_ = errors.Wrap(err, "unable to connect to database")
		return
	}
}

func prepareDb() {
	db, err := sql.Open("postgres", os.Getenv("DB_CONN_STR"))
	if err != nil {
		_ = errors.Wrap(err, "unable to connect to database")
		return
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		_ = errors.Wrap(err, "unable to connect to database")
		return
	}
	m, err := migrate.NewWithDatabaseInstance(
		"file://../migrations",
		"postgres", driver)
	if err != nil {
		_ = errors.Wrap(err, "unable to connect to database")
		return
	}
	_ = m.Up()

	cleaner = dbcleaner.New()
	ps := engine.NewPostgresEngine(os.Getenv("DB_CONN_STR"))
	cleaner.SetEngine(ps)
}

func cleanTable(t string) {
	cleaner.Clean(t)
}

func initAppForRequest(req *http.Request) *httptest.ResponseRecorder {
	a, _ := app.New()
	ap, _ := api.New(a)
	router := mux.NewRouter()
	ap.Init(router.PathPrefix("/api").Subrouter())
	router.Use(mux.CORSMethodMiddleware(router))
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	return rr
}
