package api

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/michal_hartwich/simpleblog/app"
	"bitbucket.org/michal_hartwich/simpleblog/dtos"
)

func (a *API) RegisterUser(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	userBody := &dtos.UserCreateDTO{}
	_ = json.NewDecoder(r.Body).Decode(userBody)

	errs := userBody.Validate()
	if len(errs) > 0 {
		return ctx.ValidationError(errs)
	}

	user, err := ctx.CreateUser(userBody)
	if err != nil {
		return err
	}

	serializedUser, err := json.Marshal(user)
	if err != nil {
		return err
	}
	_, err = w.Write(serializedUser)
	return err
}

func (a *API) LoginUser(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	userBody := &dtos.UserLoginDTO{}
	_ = json.NewDecoder(r.Body).Decode(userBody)

	errs := userBody.Validate()
	if len(errs) > 0 {
		return ctx.ValidationError(errs)
	}

	user, err := ctx.LoginUser(userBody)
	if err != nil {
		return err
	}

	res := map[string]string{"token": user.Token}
	serializedUser, err := json.Marshal(res)
	if err != nil {
		return err
	}
	_, err = w.Write(serializedUser)
	return err
}
