package api

import (
	"bitbucket.org/michal_hartwich/simpleblog/app"
	"github.com/NYTimes/gziphandler"
	"github.com/gorilla/mux"
)

type API struct {
	App    *app.App
	Config *Config
}

func New(a *app.App) (api *API, err error) {
	api = &API{App: a}
	api.Config, err = InitConfig()
	if err != nil {
		return nil, err
	}
	return api, nil
}

func (a *API) Init(r *mux.Router) {
	r.Handle("/hello", gziphandler.GzipHandler(a.Handler(a.RootHandler))).Methods("GET")
	r.Handle("/posts", a.Handler(a.GetPosts)).Methods("GET")
	r.Handle("/posts", a.Handler(a.CreatePost)).Methods("POST")
	r.Handle("/posts/{id}", a.Handler(a.UpdatePost)).Methods("PUT")
	r.Handle("/posts/{id}", a.Handler(a.GetPost)).Methods("GET")
	r.Handle("/posts/{id}", a.Handler(a.DeletePost)).Methods("DELETE")

	r.Handle("/auth/register", a.Handler(a.RegisterUser)).Methods("POST")
	r.Handle("/auth/login", a.Handler(a.LoginUser)).Methods("POST")
}
