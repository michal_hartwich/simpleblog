package api_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"regexp"
	"testing"
)

func TestAPI_GetPost(t *testing.T) {
	loadEnv()
	prepareDb()
	req, _ := http.NewRequest("GET", "/api/posts/1", nil)

	t.Run("when post found", func(t *testing.T) {
		cleanTable("posts")
		loadFixtures()

		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		expected := `{"id":1,"title":"some test title","content":"some test content"}`
		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when post not found", func(t *testing.T) {
		cleanTable("posts")

		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusNotFound {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNotModified)
		}
	})
}

func TestAPI_GetPosts(t *testing.T) {
	loadEnv()
	prepareDb()
	req, _ := http.NewRequest("GET", "/api/posts", nil)

	t.Run("when posts found", func(t *testing.T) {
		cleanTable("posts")
		loadFixtures()

		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		expected := `[{"id":1,"title":"some test title","content":"some test content"},{"id":2,"title":"some test title 2","content":"some test content 2"}]`
		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when posts not found", func(t *testing.T) {
		cleanTable("posts")

		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusNotFound {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNotFound)
		}
	})
}

func TestAPI_CreatePost(t *testing.T) {
	loadEnv()
	prepareDb()
	cleanTable("posts")
	loadFixtures("users")

	t.Run("with valid params", func(t *testing.T) {
		payload := map[string]string{
			"title":   "test title",
			"content": "test content",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/posts", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "test_token")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		expected := `{"id":\d+,"title":"test title","content":"test content"}`
		r, _ := regexp.Compile(expected)

		if !r.MatchString(rr.Body.String()) {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("with invalid params", func(t *testing.T) {
		payload := map[string]string{
			"title":   "",
			"content": "",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/posts", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "test_token")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusBadRequest)
		}

		expected := `{"message":"Bad request","errors":{"content":["Post content parameter cannot be empty"],"title":["Post title parameter cannot be empty"]}}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when not logged in", func(t *testing.T) {
		payload := map[string]string{
			"title":   "test title",
			"content": "test content",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/posts", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusUnauthorized {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusUnauthorized)
		}

		expected := `{"message":"You must be signed in to perform this action"}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})
}

func TestAPI_UpdatePost(t *testing.T) {
	loadEnv()
	prepareDb()
	loadFixtures()

	t.Run("with valid params", func(t *testing.T) {
		payload := map[string]string{
			"title":   "test modified title",
			"content": "test modified content",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("PUT", "/api/posts/1", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "test_token")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		expected := `{"id":\d+,"title":"test modified title","content":"test modified content"}`
		r, _ := regexp.Compile(expected)

		if !r.MatchString(rr.Body.String()) {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("with invalid params", func(t *testing.T) {
		payload := map[string]string{
			"title":   "",
			"content": "",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("PUT", "/api/posts/1", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "test_token")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusBadRequest)
		}

		expected := `{"message":"Bad request","errors":{"content":["Post content parameter cannot be empty"],"title":["Post title parameter cannot be empty"]}}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when post doesn't exist", func(t *testing.T) {
		payload := map[string]string{
			"title":   "test modified title",
			"content": "test modified content",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("PUT", "/api/posts/33333", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "test_token")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusNotFound {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNotFound)
		}

		expected := `{"message":"Record not found"}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when not logged in", func(t *testing.T) {
		payload := map[string]string{
			"title":   "test title",
			"content": "test content",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/posts", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusUnauthorized {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusUnauthorized)
		}

		expected := `{"message":"You must be signed in to perform this action"}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})
}

func TestAPI_DeletePost(t *testing.T) {
	loadEnv()
	prepareDb()
	loadFixtures()

	t.Run("with valid params", func(t *testing.T) {
		req, _ := http.NewRequest("DELETE", "/api/posts/1", nil)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "test_token")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		if rr.Body.String() != "" {
			t.Errorf("handler returned unexpected body: got %v want \"\"",
				rr.Body.String())
		}
	})

	t.Run("when post doesn't exist", func(t *testing.T) {
		req, _ := http.NewRequest("DELETE", "/api/posts/33333", nil)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "test_token")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusNotFound {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNotFound)
		}

		expected := `{"message":"Record not found"}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})

	t.Run("when not logged in", func(t *testing.T) {
		payload := map[string]string{
			"title":   "test title",
			"content": "test content",
		}
		payloadJson, _ := json.Marshal(payload)
		req, _ := http.NewRequest("POST", "/api/posts", bytes.NewBuffer(payloadJson))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-USER-TOKEN", "")
		rr := initAppForRequest(req)

		if status := rr.Code; status != http.StatusUnauthorized {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusUnauthorized)
		}

		expected := `{"message":"You must be signed in to perform this action"}`

		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}
	})
}
