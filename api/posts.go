package api

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/michal_hartwich/simpleblog/app"
	"bitbucket.org/michal_hartwich/simpleblog/dtos"
	"github.com/gorilla/mux"
)

func (a *API) GetPosts(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	posts, err := ctx.GetPosts()
	if err != nil {
		return err
	}

	serializedPosts, err := json.Marshal(posts)
	if err != nil {
		return err
	}

	_, err = w.Write(serializedPosts)
	return err
}

func (a *API) GetPost(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	postId, _ := strconv.Atoi(mux.Vars(r)["id"])
	post, err := ctx.GetPost(postId)
	if err != nil {
		return err
	}

	serializedPost, err := json.Marshal(post)
	if err != nil {
		return err
	}

	_, err = w.Write(serializedPost)
	return err
}

func (a *API) CreatePost(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	if ctx.CurrentUser == nil {
		return ctx.NotLoggedInError()
	}

	postBody := &dtos.PostDTO{}

	_ = json.NewDecoder(r.Body).Decode(postBody)

	errs := postBody.Validate()
	if len(errs) > 0 {
		return ctx.ValidationError(errs)
	}

	post, _ := ctx.CreatePost(postBody)

	serializedPost, err := json.Marshal(post)
	if err != nil {
		return err
	}

	_, err = w.Write(serializedPost)
	return err
}

func (a *API) UpdatePost(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	if ctx.CurrentUser == nil {
		return ctx.NotLoggedInError()
	}

	postBody := &dtos.PostDTO{}
	postId, _ := strconv.Atoi(mux.Vars(r)["id"])

	_ = json.NewDecoder(r.Body).Decode(postBody)

	errs := postBody.Validate()
	if len(errs) > 0 {
		return ctx.ValidationError(errs)
	}

	post, err := ctx.UpdatePost(postId, postBody)
	if err != nil {
		return err
	}

	serializedPost, err := json.Marshal(post)
	if err != nil {
		return err
	}

	_, err = w.Write(serializedPost)
	return err
}

func (a *API) DeletePost(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	if ctx.CurrentUser == nil {
		return ctx.NotLoggedInError()
	}

	postId, _ := strconv.Atoi(mux.Vars(r)["id"])

	err := ctx.DeletePost(postId)
	if err != nil {
		return err
	}
	return nil
}
