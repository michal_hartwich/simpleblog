package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"runtime/debug"
	"strings"
	"time"

	"bitbucket.org/michal_hartwich/simpleblog/app"
	"github.com/sirupsen/logrus"
)

type statusCodeRecorder struct {
	http.ResponseWriter
	http.Hijacker
	StatusCode int
}

func (r *statusCodeRecorder) WriteHeader(statusCode int) {
	r.StatusCode = statusCode
	r.ResponseWriter.WriteHeader(statusCode)
}

func (a *API) Handler(f func(*app.Context, http.ResponseWriter, *http.Request) error) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.Body = http.MaxBytesReader(w, r.Body, 100*1024*1024)
		beginTime := time.Now()

		hijacker, _ := w.(http.Hijacker)
		w = &statusCodeRecorder{
			ResponseWriter: w,
			Hijacker:       hijacker,
		}
		ctx := a.App.NewContext()
		defer a.addLogger(ctx, w, r, beginTime)
		defer a.addErrorCatching(ctx, w, r)
		a.authUser(ctx, r)

		a.setHeaders(w)
		if err := f(ctx, w, r); err != nil {
			switch err.(type) {
			case *app.ValidationError:
				data, _ := json.Marshal(err)
				w.WriteHeader(http.StatusBadRequest)
				_, _ = w.Write(data)
			case *app.RecordNotFoundError:
				data, _ := json.Marshal(err)
				w.WriteHeader(http.StatusNotFound)
				_, _ = w.Write(data)
			case *app.UnprocessableEntityError:
				data, _ := json.Marshal(err)
				w.WriteHeader(http.StatusUnprocessableEntity)
				_, _ = w.Write(data)
			case *app.UnauthorizedError:
				data, _ := json.Marshal(err)
				w.WriteHeader(http.StatusUnauthorized)
				_, _ = w.Write(data)
			case *app.ForbiddenError:
				data, _ := json.Marshal(err)
				w.WriteHeader(http.StatusForbidden)
				_, _ = w.Write(data)
			default:
				ctx.Logger.Error(err)
				http.Error(w, "internal server error", http.StatusInternalServerError)
			}
			return
		}
	}
}

func (a *API) addLogger(ctx *app.Context, w http.ResponseWriter, r *http.Request, beginTime time.Time) {
	ctx.WithRemoteAddress(a.IPAddressForRequest(r))
	ctx = ctx.WithLogger(ctx.Logger.WithField("at", time.Now().Format(time.RFC3339)))
	statusCode := w.(*statusCodeRecorder).StatusCode
	if statusCode == 0 {
		statusCode = 200
	}
	duration := time.Since(beginTime)

	logger := ctx.Logger.WithFields(logrus.Fields{
		"duration":    duration,
		"status_code": statusCode,
		"remote":      ctx.RemoteAddress,
	})
	if statusCode >= 400 {
		logger.Warn(r.Method + " " + r.URL.RequestURI())
	} else if statusCode >= 500 {
		logger.Error(r.Method + " " + r.URL.RequestURI())
	} else {
		logger.Info(r.Method + " " + r.URL.RequestURI())
	}
}

func (a *API) addErrorCatching(ctx *app.Context, w http.ResponseWriter, r *http.Request) {
	if r := recover(); r != nil {
		ctx.Logger.Error(fmt.Errorf("%v: %s", r, debug.Stack()))
		http.Error(w, "internal server error", http.StatusInternalServerError)
	}
}

func (a *API) setHeaders(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
}

func (a *API) RootHandler(ctx *app.Context, w http.ResponseWriter, r *http.Request) error {
	_, err := w.Write([]byte(`{"hello" : "world"}`))
	return err
}

func (a *API) authUser(ctx *app.Context, r *http.Request) {
	token := r.Header.Get("X-USER-TOKEN")
	user, err := ctx.Db.GetUserByToken(token)
	if err != nil {
		return
	}
	ctx.CurrentUser = user
}

func (a *API) IPAddressForRequest(r *http.Request) string {
	addr := r.RemoteAddr
	if a.Config.ProxyCount > 0 {
		h := r.Header.Get("X-Forwarded-For")
		if h != "" {
			clients := strings.Split(h, ",")
			if a.Config.ProxyCount > len(clients) {
				addr = clients[0]
			} else {
				addr = clients[len(clients)-a.Config.ProxyCount]
			}
		}
	}
	return strings.Split(strings.TrimSpace(addr), ":")[0]
}
