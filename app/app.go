package app

import (
	"bitbucket.org/michal_hartwich/simpleblog/db"
	"github.com/sirupsen/logrus"
)

type App struct {
	Config *Config
	Db		 db.Repository
}

func (a *App) NewContext() *Context {
	return &Context{
		Logger: logrus.New(),
		Db: a.Db,
		//GetPost: getPost,
		//GetPosts: getPosts,
		//CreatePost: createPost,
		//UpdatePost: updatePost,
		//DeletePost: deletePost,
	}
}

func New() (app *App, err error) {
	app = &App{}
	app.Config, err = InitConfig()

	dbConfig, err := db.InitConfig()
	if err != nil {
		return nil, err
	}

	app.Db, err = db.New(dbConfig)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}
	return app, err
}

func (a *App) Close() error {
	return a.Db.Close()
}
