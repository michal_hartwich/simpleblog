package app

import (
	"fmt"
	"os"
)

type Config struct {
	SecretKey    []byte
	DbConnString string
}

func InitConfig() (*Config, error) {
	config := &Config{
		SecretKey: []byte(os.Getenv("SECRET_KEY")),
	}
	if len(config.SecretKey) == 0 {
		return nil, fmt.Errorf("SecretKey must be set!")
	}
	return config, nil
}
