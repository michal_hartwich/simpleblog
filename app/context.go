package app

import (
	"database/sql"
	"net/url"

	"bitbucket.org/michal_hartwich/simpleblog/db"
	"bitbucket.org/michal_hartwich/simpleblog/models"
	"github.com/sirupsen/logrus"
)

type Context struct {
	Logger        logrus.FieldLogger
	CurrentUser   *models.User
	RemoteAddress string
	Db            db.Repository
	//GetPost				func(ctx *Context, id int) (*models.Post, error)
	//GetPosts			func(ctx *Context) ([]*models.Post, error)
	//CreatePost		func(ctx *Context, p *dtos.PostDTO) (*models.Post, error)
	//UpdatePost		func(ctx *Context, id int, p *dtos.PostDTO) (*models.Post, error)
	//DeletePost		func(ctx *Context, id int) error
}

func (ctx *Context) WithLogger(logger logrus.FieldLogger) *Context {
	ret := *ctx
	ret.Logger = logger
	return &ret
}

func (ctx *Context) WithRemoteAddress(address string) *Context {
	ret := *ctx
	ret.RemoteAddress = address
	return &ret
}

func (ctx *Context) ValidationError(errs url.Values) *ValidationError {
	return &ValidationError{"Bad request", errs}
}

func (ctx *Context) RecordNotFoundError() *RecordNotFoundError {
	return &RecordNotFoundError{"Record not found"}
}

func (ctx *Context) UnprocessableEntityError(msg string) *UnprocessableEntityError {
	return &UnprocessableEntityError{msg}
}

func (ctx *Context) UnauthorizedError(msg string) *UnauthorizedError {
	return &UnauthorizedError{msg}
}

func (ctx *Context) NotLoggedInError() *UnauthorizedError {
	return &UnauthorizedError{"You must be signed in to perform this action"}
}

func (ctx *Context) ForbiddenError() *UnauthorizedError {
	return &UnauthorizedError{"You are not allowed to perform this action"}
}

func (ctx *Context) RaiseError(err error) error {
	if err == sql.ErrNoRows {
		return ctx.RecordNotFoundError()
	}
	return err
}
