package app

import (
	"bitbucket.org/michal_hartwich/simpleblog/app/policies"
	"bitbucket.org/michal_hartwich/simpleblog/dtos"
	"bitbucket.org/michal_hartwich/simpleblog/models"
)

func (ctx *Context) GetPosts() ([]*models.Post, error) {
	posts, err := ctx.Db.GetPosts()
	if err != nil {
		return nil, ctx.RaiseError(err)
	}

	return posts, nil
}

func (ctx *Context) GetPost(id int) (*models.Post, error) {
	post, err := ctx.Db.GetPost(id)
	if err != nil {
		return nil, ctx.RaiseError(err)
	}

	return post, nil
}

func (ctx *Context) CreatePost(p *dtos.PostDTO) (*models.Post, error) {
	post := &models.Post{
		Title:   p.Title,
		Content: p.Content,
		UserID:  ctx.CurrentUser.ID,
	}

	post, err := ctx.Db.CreatePost(post)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (ctx *Context) UpdatePost(id int, p *dtos.PostDTO) (*models.Post, error) {
	post, err := ctx.Db.GetPost(id)
	if err != nil {
		return nil, ctx.RaiseError(err)
	}

	postPolicy := &policies.PostPolicy{User: ctx.CurrentUser, Post: post}
	if !postPolicy.CanUpdate() {
		return nil, ctx.ForbiddenError()
	}

	post.Title = p.Title
	post.Content = p.Content

	post, err = ctx.Db.UpdatePost(post)
	if err != nil {
		return nil, ctx.RaiseError(err)
	}

	return post, nil
}

func (ctx *Context) DeletePost(id int) error {
	post, err := ctx.Db.GetPost(id)
	if err != nil {
		return ctx.RaiseError(err)
	}

	postPolicy := &policies.PostPolicy{User: ctx.CurrentUser, Post: post}
	if !postPolicy.CanDelete() {
		return ctx.ForbiddenError()
	}

	err = ctx.Db.DeletePost(id)
	if err != nil {
		return ctx.RaiseError(err)
	}

	return nil
}
