package app

import (
	"crypto/rand"
	"database/sql"
	"fmt"

	"bitbucket.org/michal_hartwich/simpleblog/dtos"
	"bitbucket.org/michal_hartwich/simpleblog/models"
	"golang.org/x/crypto/bcrypt"
)

func (ctx *Context) CreateUser(u *dtos.UserCreateDTO) (*models.User, error) {
	existingUser, err := ctx.Db.GetUserByEmail(u.Email)
	if existingUser != nil {
		return nil, ctx.UnprocessableEntityError("User with given email already exists")
	}
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.MinCost)
	if err != nil {
		return nil, err
	}

	user := &models.User{
		Email:    u.Email,
		Password: string(hashedPassword),
	}

	user, err = ctx.Db.CreateUser(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (ctx *Context) LoginUser(u *dtos.UserLoginDTO) (*models.User, error) {
	user, err := ctx.Db.GetUserByEmail(u.Email)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	if user == nil {
		return nil, ctx.UnauthorizedError("Invalid email or password")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(u.Password))
	if err != nil {
		return nil, ctx.UnauthorizedError("Invalid email or password")
	}

	user.Token = randToken()
	err = ctx.Db.SetUserToken(user.ID, user.Token)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func randToken() string {
	b := make([]byte, 20)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}
