package app_test

import (
	"database/sql"
	"errors"
	"testing"

	"bitbucket.org/michal_hartwich/simpleblog/app"
	"bitbucket.org/michal_hartwich/simpleblog/db"
	"bitbucket.org/michal_hartwich/simpleblog/dtos"
	"bitbucket.org/michal_hartwich/simpleblog/models"
)

type createUserDbMock1 struct {
	db.Repository
}

func (db *createUserDbMock1) CreateUser(u *models.User) (*models.User, error) {
	u.ID = 1
	return u, nil
}

func (db *createUserDbMock1) GetUserByEmail(email string) (*models.User, error) {
	return nil, sql.ErrNoRows
}

type createUserDbMock2 struct {
	db.Repository
}

func (db *createUserDbMock2) GetUserByEmail(email string) (*models.User, error) {
	return &models.User{ID: 1}, nil
}

type createUserDbMock3 struct {
	db.Repository
}

func (db *createUserDbMock3) GetUserByEmail(email string) (*models.User, error) {
	return nil, errors.New("some error")
}

func TestContext_CreateUser(t *testing.T) {
	u := &dtos.UserCreateDTO{
		Email:                "user@example.com",
		Password:             "userpass",
		PasswordConfirmation: "userpass",
	}

	t.Run("when user successfully created", func(t *testing.T) {
		ctx := &app.Context{Db: &createUserDbMock1{}}

		res, err := ctx.CreateUser(u)

		if err != nil {
			t.Errorf("Error shouldn't be thrown, got: %v, want: nil", err)
		}
		if res.Email != u.Email || res.ID == 0 {
			t.Errorf("User object not created properly, got: %+v, want: %+v", res, u)
		}
	})

	t.Run("when user with given email exists", func(t *testing.T) {
		ctx := &app.Context{Db: &createUserDbMock2{}}

		res, err := ctx.CreateUser(u)

		if res != nil {
			t.Errorf("Result should be nil, got: %+v, want: nil.", res)
		}
		if _, ok := err.(*app.UnprocessableEntityError); !ok {
			t.Errorf("Wrong error returned, got: %T, want: %T.", &err, app.RecordNotFoundError{})
		}
	})

	t.Run("when error occurred", func(t *testing.T) {
		ctx := &app.Context{Db: &createUserDbMock3{}}

		res, err := ctx.CreateUser(u)

		if res != nil {
			t.Errorf("Post should be nil, got: %+v, want: nil.", res)
		}
		if err == nil {
			t.Errorf("Error should be thrown, got: nil, want: *error")
		}
	})
}

type loginUserDbMock1 struct {
	db.Repository
}

func (db *loginUserDbMock1) GetUserByEmail(email string) (*models.User, error) {
	return &models.User{
		ID:       1,
		Email:    "user@example.com",
		Password: "$2a$04$E8.r8Xiz.WlegEq.F.BB7eXiQWhUeaRdoo573LVwrU4gubuIYEIua",
	}, nil
}

func (db *loginUserDbMock1) SetUserToken(id int, token string) error {
	return nil
}

type loginUserDbMock2 struct {
	db.Repository
}

func (db *loginUserDbMock2) GetUserByEmail(email string) (*models.User, error) {
	return nil, sql.ErrNoRows
}

func TestContext_LoginUser(t *testing.T) {
	u := &dtos.UserLoginDTO{
		Email:    "user@example.com",
		Password: "userpass",
	}

	t.Run("when logged in successfully", func(t *testing.T) {
		ctx := &app.Context{Db: &loginUserDbMock1{}}

		res, err := ctx.LoginUser(u)

		if err != nil {
			t.Errorf("Error shouldn't be thrown, got: %v, want: nil", err)
		}
		if res.Token == "" {
			t.Errorf("User object not created properly, got: %+v, want: %+v", res, u)
		}
	})

	t.Run("when user doesn't exist", func(t *testing.T) {
		ctx := &app.Context{Db: &loginUserDbMock2{}}

		res, err := ctx.LoginUser(u)

		if res != nil {
			t.Errorf("Result should be nil, got: %+v, want: nil.", res)
		}
		if _, ok := err.(*app.UnauthorizedError); !ok {
			t.Errorf("Wrong error returned, got: %T, want: %T.", &err, app.RecordNotFoundError{})
		}
	})

	t.Run("when password is invalid", func(t *testing.T) {
		ctx := &app.Context{Db: &loginUserDbMock1{}}

		u.Password = "wrongpass"
		res, err := ctx.LoginUser(u)

		if res != nil {
			t.Errorf("Result should be nil, got: %+v, want: nil.", res)
		}
		if _, ok := err.(*app.UnauthorizedError); !ok {
			t.Errorf("Wrong error returned, got: %T, want: %T.", &err, app.RecordNotFoundError{})
		}
	})
}
