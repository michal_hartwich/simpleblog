package policies

import "bitbucket.org/michal_hartwich/simpleblog/models"

type PostPolicy struct {
	User *models.User
	Post *models.Post
}

func (p *PostPolicy) CanUpdate() bool {
	return p.User.ID == p.Post.UserID
}

func (p *PostPolicy) CanDelete() bool {
	return p.User.ID == p.Post.UserID
}
