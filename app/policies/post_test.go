package policies_test

import (
	"testing"

	"bitbucket.org/michal_hartwich/simpleblog/app/policies"
	"bitbucket.org/michal_hartwich/simpleblog/models"
)

func TestPostPolicy_CanUpdate(t *testing.T) {
	user := &models.User{ID: 1}
	post := &models.Post{ID: 1, UserID: user.ID}

	t.Run("when user is owner", func(t *testing.T) {
		postPolicy := policies.PostPolicy{Post: post, User: user}
		res := postPolicy.CanUpdate()

		if !res {
			t.Errorf("Wrong result received, got: false, want: true")
		}
	})

	t.Run("when user is not an owner", func(t *testing.T) {
		post.UserID = 22
		postPolicy := policies.PostPolicy{Post: post, User: user}
		res := postPolicy.CanUpdate()

		if res {
			t.Errorf("Wrong result received, got: true, want: false")
		}
	})
}

func TestPostPolicy_CanDelete(t *testing.T) {
	user := &models.User{ID: 1}
	post := &models.Post{ID: 1, UserID: user.ID}

	t.Run("when user is owner", func(t *testing.T) {
		postPolicy := policies.PostPolicy{Post: post, User: user}
		res := postPolicy.CanDelete()

		if !res {
			t.Errorf("Wrong result received, got: false, want: true")
		}
	})

	t.Run("when user is not an owner", func(t *testing.T) {
		post.UserID = 22
		postPolicy := policies.PostPolicy{Post: post, User: user}
		res := postPolicy.CanDelete()

		if res {
			t.Errorf("Wrong result received, got: true, want: false")
		}
	})
}
