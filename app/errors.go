package app

import "net/url"

type ValidationError struct {
	Message string     `json:"message"`
	Details url.Values `json:"errors"`
}

func (e *ValidationError) Error() string {
	return e.Message
}

type RecordNotFoundError struct {
	Message string `json:"message"`
}

func (e *RecordNotFoundError) Error() string {
	return e.Message
}

type UnprocessableEntityError struct {
	Message string `json:"message"`
}

func (e *UnprocessableEntityError) Error() string {
	return e.Message
}

type UnauthorizedError struct {
	Message string `json:"message"`
}

func (e *UnauthorizedError) Error() string {
	return e.Message
}

type ForbiddenError struct {
	Message string `json:"message"`
}

func (e *ForbiddenError) Error() string {
	return e.Message
}
