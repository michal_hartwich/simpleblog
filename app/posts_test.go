package app_test

import (
  "bitbucket.org/michal_hartwich/simpleblog/app"
  "bitbucket.org/michal_hartwich/simpleblog/db"
  "bitbucket.org/michal_hartwich/simpleblog/dtos"
  "bitbucket.org/michal_hartwich/simpleblog/models"
  "database/sql"
  "errors"
  "fmt"
  "testing"
)

// GetPosts() test cases

type getPostDbMock1 struct {
  db.Repository
}

func (m *getPostDbMock1) GetPost(id int) (*models.Post, error) {
  return &models.Post{
    ID:      1,
    Title:   "test title",
    Content: "test content",
  }, nil
}

type getPostDbMock2 struct {
  db.Repository
}

func (m *getPostDbMock2) GetPost(id int) (*models.Post, error) {
  return nil, sql.ErrNoRows
}

type getPostDbMock3 struct {
  db.Repository
}

func (m *getPostDbMock3) GetPost(id int) (*models.Post, error) {
  return nil, errors.New("some error")
}

func TestApp_GetPost(t *testing.T) {
  t.Run("when post exists", func(t *testing.T) {
    ctx := &app.Context{Db: &getPostDbMock1{}}
    res, _ := ctx.GetPost(1)

    if res == nil {
      t.Errorf("Post was incorrect, got: nil, want: *model.Post.")
    }
  })

  t.Run("when post doesn't exist", func(t *testing.T) {
    ctx := &app.Context{Db: &getPostDbMock2{}}
    res, err := ctx.GetPost(2)

    if res !=nil {
      t.Errorf("Post should be nil, got: %d, want: nil.", res.ID)
    }
    if _, ok := err.(*app.RecordNotFoundError); !ok {
      t.Errorf("Wrong error returned, got: %T, want: %T.", &err, app.RecordNotFoundError{})
    }
  })

  t.Run("when error occurred", func(t *testing.T) {
    ctx := &app.Context{Db: &getPostDbMock3{}}
    res, err := ctx.GetPost(3)

    if res !=nil {
      t.Errorf("Post should be nil, got: %d, want: nil.", res.ID)
    }
    if err == nil {
      t.Errorf("Error should be thrown, got: nil, want: *error")
    }
  })
}


// GetPosts() test cases

const getPostsResultLen = 3

type getPostsDbMock1 struct {
  db.Repository
}

func (m *getPostsDbMock1) GetPosts() ([]*models.Post, error) {
  posts := make([]*models.Post, 0)
  for i := 0; i < getPostsResultLen; i++ {
    posts = append(posts, &models.Post{
      ID:      i,
      Title:    fmt.Sprintf("some title %d", i),
      Content:  fmt.Sprintf("some content %d", i),
    })
  }
  return posts, nil
}

type getPostsDbMock2 struct {
  db.Repository
}

func (m *getPostsDbMock2) GetPosts() ([]*models.Post, error) {
  return nil, sql.ErrNoRows
}

type getPostsDbMock3 struct {
  db.Repository
}

func (m *getPostsDbMock3) GetPosts() ([]*models.Post, error) {
  return nil, errors.New("some error")
}

func TestContext_GetPosts(t *testing.T) {
  t.Run("when posts exist", func(t *testing.T) {
    ctx := &app.Context{Db: &getPostsDbMock1{}}
    res, _ := ctx.GetPosts()

    if len(res) != 3 {
      t.Errorf("Returned posts array has wrong size, got: %d, want: %d", len(res), getPostsResultLen)
    }
  })

  t.Run("when posts don't exist", func(t *testing.T) {
    ctx := &app.Context{Db: &getPostsDbMock2{}}
    res, err := ctx.GetPosts()

    if res != nil {
      t.Errorf("Result should be nil, got: %d, want: nil.", len(res))
    }
    if _, ok := err.(*app.RecordNotFoundError); !ok {
      t.Errorf("Wrong error returned, got: %T, want: %T.", &err, app.RecordNotFoundError{})
    }
  })

  t.Run("when error occurred", func(t *testing.T) {
    ctx := &app.Context{Db: &getPostsDbMock3{}}
    res, err := ctx.GetPosts()

    if res !=nil {
      t.Errorf("Posts should be nil, got: %d, want: nil.", len(res))
    }
    if err == nil {
      t.Errorf("Error should be thrown, got: nil, want: *error")
    }
  })
}

// CreatePost() test cases

type createPostDbMock1 struct {
  db.Repository
}

func (db *createPostDbMock1) CreatePost(p *models.Post) (*models.Post, error)  {
  p.ID = 1
  return p, nil
}

type createPostDbMock2 struct {
  db.Repository
}

func (db *createPostDbMock2) CreatePost(p *models.Post) (*models.Post, error)  {
  return nil, errors.New("some error")
}

func TestContext_CreatePost(t *testing.T) {
  t.Run("when post successfully created", func(t *testing.T) {
    ctx := &app.Context{Db: &createPostDbMock1{}}

    p := &dtos.PostDTO{
      Title:   "some test title",
      Content: "some test content",
    }

    res, err := ctx.CreatePost(p)

    if err != nil {
      t.Errorf("Error shouldn't be thrown, got: %s, want: nil", err)
    }
    if res.Title != p.Title || res.Content != p.Content || res.ID == 0 {
      t.Errorf("Post object not created properly, got: %+v, want: %+v", res, p)
    }
  })

  t.Run("when error occured", func(t *testing.T) {
    ctx := &app.Context{Db: &createPostDbMock2{}}

    p := &dtos.PostDTO{
      Title:   "some test title",
      Content: "some test content",
    }

    res, err := ctx.CreatePost(p)

    if res != nil {
      t.Errorf("Posts should be nil, got: %+v, want: nil.", res)
    }
    if err == nil {
      t.Errorf("Error should be thrown, got: nil, want: *error")
    }
  })
}

// UpdatePost() test cases

type updatePostDbMock1 struct {
  db.Repository
}

func (db *updatePostDbMock1) UpdatePost(p *models.Post) (*models.Post, error)  {
  return p, nil
}

type updatePostDbMock2 struct {
  db.Repository
}

func (db *updatePostDbMock2) UpdatePost(p *models.Post) (*models.Post, error)  {
  return nil, sql.ErrNoRows
}

type updatePostDbMock3 struct {
  db.Repository
}

func (db *updatePostDbMock3) UpdatePost(p *models.Post) (*models.Post, error)  {
  return nil, errors.New("some error")
}

func TestContext_UpdatePost(t *testing.T) {
  postId := 22
  p := &dtos.PostDTO{
    Title:   "some test title",
    Content: "some test content",
  }

  t.Run("when successfully updated", func(t *testing.T) {
    ctx := &app.Context{Db: &updatePostDbMock1{}}

    res, err := ctx.UpdatePost(postId, p)

    if err != nil {
      t.Errorf("Error shouldn't be thrown, got: %s, want: nil", err)
    }
    if res.ID != postId {
      t.Errorf("Post id not assigned properly, got: %d, want: %d", res.ID, postId)
    }
  })

  t.Run("when record not found", func(t *testing.T) {
    ctx := &app.Context{Db: &updatePostDbMock2{}}

    res, err := ctx.UpdatePost(postId, p)

    if res != nil {
      t.Errorf("Result should be nil, got: %+v, want: nil.", res)
    }
    if _, ok := err.(*app.RecordNotFoundError); !ok {
      t.Errorf("Wrong error returned, got: %T, want: %T.", &err, app.RecordNotFoundError{})
    }
  })

  t.Run("when error occurred", func(t *testing.T) {
    ctx := &app.Context{Db: &updatePostDbMock3{}}

    res, err := ctx.UpdatePost(postId, p)

    if res != nil {
      t.Errorf("Post should be nil, got: %+v, want: nil.", res)
    }
    if err == nil {
      t.Errorf("Error should be thrown, got: nil, want: *error")
    }
  })
}

// DeletePost() test cases

type deletePostDbMock1 struct {
  db.Repository
}

func (db *deletePostDbMock1) DeletePost(id int) error {
  return nil
}

type deletePostDbMock2 struct {
  db.Repository
}

func (db *deletePostDbMock2) DeletePost(id int) error {
  return sql.ErrNoRows
}

type deletePostDbMock3 struct {
  db.Repository
}

func (db *deletePostDbMock3) DeletePost(id int) error {
  return errors.New("some error")
}


func TestContext_DeletePost(t *testing.T) {
 t.Run("when successfully deleted", func(t *testing.T) {
   ctx := &app.Context{Db: &deletePostDbMock1{}}

   res := ctx.DeletePost(1)

   if res != nil {
     t.Errorf("Result should be nil, got: %+v, want: nil.", res)
   }
 })

 t.Run("when record not found", func(t *testing.T) {
   ctx := &app.Context{Db: &deletePostDbMock2{}}

   res := ctx.DeletePost(1)

   if _, ok := res.(*app.RecordNotFoundError); !ok {
     t.Errorf("Wrong error returned, got: %T, want: %T.", &res, app.RecordNotFoundError{})
   }
 })

 t.Run("when error occurred", func(t *testing.T) {
   ctx := &app.Context{Db: &deletePostDbMock3{}}

   res := ctx.DeletePost(1)

   if res == nil {
     t.Errorf("Error should be thrown, got: nil, want: *error")
   }
 })
}