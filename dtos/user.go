package dtos

import "net/url"

type UserCreateDTO struct {
	Email                string `json:"email"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"password_confirmation"`
}

func (u *UserCreateDTO) Validate() url.Values {
	errs := url.Values{}

	if u.Password != u.PasswordConfirmation {
		errs.Add("password", "Password and password confirmation don't match")
	}

	if u.Email == "" {
		errs.Add("email", "Email param cannot be empty")
	}

	return errs
}

type UserLoginDTO struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u *UserLoginDTO) Validate() url.Values {
	errs := url.Values{}

	if u.Password == "" {
		errs.Add("password", "Password param cannot be empty")
	}

	if u.Email == "" {
		errs.Add("email", "Email param cannot be empty")
	}

	return errs
}
