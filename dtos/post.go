package dtos

import "net/url"

type PostDTO struct {
	Title   string `json:"title"`
	Content string `json:"content"`
}

func (p *PostDTO) Validate() url.Values {
	errs := url.Values{}

	if p.Title == "" {
		errs.Add("title", "Post title parameter cannot be empty")
	}
	if p.Content == "" {
		errs.Add("content", "Post content parameter cannot be empty")
	}

	return errs
}
