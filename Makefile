migrate_up:
	migrate -database postgres://localhost:5432/go_blog?sslmode=disable -path migrations up

migrate_down:
	migrate -database postgres://localhost:5432/go_blog?sslmode=disable -path migrations down $(N)